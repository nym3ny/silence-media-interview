--
-- Database: `silence_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `landmark`
--

CREATE TABLE `landmark` (
  `id` int(8) NOT NULL,
  `name` varchar(255) NOT NULL,
  `latitude` varchar(10) NOT NULL,
  `longitude` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `landmark`
--

INSERT INTO `landmark` (`id`, `name`, `latitude`, `longitude`) VALUES
(1, 'The British Museum', '51.5194133', '-0.1291453'),
(2, 'SEA LIFE London Aquarium', '51.5020103', '-0.1215078'),
(3, 'Big Ben', '51.5007292', '-0.1268141'),
(4, 'Buckingham Palace', '51.501364', '-0.1440787'),
(5, 'Tower Bridge', '51.5054564', '-0.0775452'),
(6, 'London Eye', '51.503324', '-0.1217317');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `landmark`
--
ALTER TABLE `landmark`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `landmark`
--
ALTER TABLE `landmark`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;