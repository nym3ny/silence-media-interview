var API_PATH = 'yii.local.ro';

$(document).ready(function(){

    //init google map
    function initMap(){
        $('#map').googleMap({
            zoom: 12, // Initial zoom level (optional)
            coords: [51.4976548,-0.129104], // Map center (Center of London)
            type: "ROADMAP" // Map type (optional)
        });
    }
    initMap();

    //init landmarks panel
    $.ajax({

        url: 'http://'+ API_PATH +'/landmark',
        dataType: 'json',
        success:function(data){
            console.log(data);
            if(data.length > 0){

                data.forEach(function(landmark){

                    $("#landmarks").append('<p>'+ landmark.name +'</p>');

                });

            }else{
                alert('No landmark found!');
            }
        },
        error:function(){
            alert('Unable to connect on api!');
        }

    });

    //Bind search button
    $('#search').submit(function(e){
        e.preventDefault();
        initMap();

        $.ajax({

            url: 'http://'+ API_PATH +'/landmark/search?keyword=' + encodeURIComponent($('#keyword').val()),
            dataType: 'json',
            success:function(data){
                console.log(data);
                if(data.length > 0){

                    data.forEach(function(landmark){

                        //Add landmark to map
                        $('#map').addMarker({
                            coords: [landmark.latitude,landmark.longitude], // GPS coords
                            title: landmark.name , // Title
                            text: '<p></p>', //html content popup
                        });

                    });

                }else{
                    alert('No landmark found!');
                }
            },
            error:function(){
                alert('Unable to connect on api!');
            }

        });

    });

});


