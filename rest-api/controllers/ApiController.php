<?php

namespace app\controllers;


class ApiController extends \yii\rest\ActiveController
{

    public $modelClass = '\app\models\ApiModel';

    public function behaviors()
    {
        return [

            'contentNegotiator' => [
                'class' 			=> \yii\filters\ContentNegotiator::className(),

                'formats' 			=> [
                    'text/html' 			=> \yii\web\Response::FORMAT_JSON,
                    'application/json' 			=> \yii\web\Response::FORMAT_JSON,
                    'application/javascript' 	=> \yii\web\Response::FORMAT_JSONP,
                    'application/xml' 			=> \yii\web\Response::FORMAT_XML,
                ],
            ],
        ];
    }

    public function actions(){

    }


}








