<?php

namespace app\controllers;

class LandmarkController extends ApiController
{

    public $modelClass = '\app\models\Landmark';

    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => \yii\filters\AccessControl::className(),
                    'only' => ['index'],
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                        ],
                    ],
                ],
                'verbs' => [
                    'class' => \yii\filters\VerbFilter::className(),
                    'actions' => [
                        'index'  => ['get'],
                        'view'   => ['get'],
                        'create' => ['post'],
                        'update' => ['put', 'post'],
                        'delete' => ['delete'],
                        'search' => ['get']
                    ],
                ],
            ]);
    }


    public function actionIndex(){

        $landmark = \app\models\Landmark::find()->all();
        return $landmark;

    }

    public function actionSearch(){

        if(!empty(\Yii::$app->request->get('keyword'))){

            $landmark = \app\models\Landmark::find()->where('name LIKE "%'.\Yii::$app->request->get('keyword').'%"')->all();
            return $landmark;

        }else{

            return [];

        }


    }

    public function actionView(){

        if(!empty(\Yii::$app->request->get('id'))){

            $landmark = \app\models\Landmark::find()->where('id = '.\Yii::$app->request->get('id'))->all();
            return $landmark;

        }else{

            return [];

        }

    }

}
