<?php

namespace app\controllers;

class SiteController extends ApiController
{

    public $modelClass = '\app\models\Landmark';

    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => \yii\filters\AccessControl::className(),
                    'only' => ['index'],
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                        ],
                    ],
                ],
                'verbs' => [
                    'class' => \yii\filters\VerbFilter::className(),
                    'actions' => [
                        'index'  => ['get'],
                        'view'   => ['get'],
                        'create' => ['post'],
                        'update' => ['put', 'post'],
                        'delete' => ['delete'],
                        'search' => ['get']
                    ],
                ],
            ]);
    }


    public function actionIndex()
    {
        return [
            'message' => 'Welcome to yii2 rest api',
            'routes'  => [
                'landmark' => [
                    'index', 'view', 'search'
                ]
            ]
        ];
    }



}
