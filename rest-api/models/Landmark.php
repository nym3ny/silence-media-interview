<?php

namespace app\models;


/**
 * This is the model class for table "landmark".
 *
 * @property integer $id
 * @property string $name
 * @property string $latitude
 * @property string $longitude
 */
class Landmark extends ApiModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'landmark';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'latitude', 'longitude'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['latitude', 'longitude'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
        ];
    }
}
