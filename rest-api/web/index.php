<?php
//vendorPath
define('VENDOR_PATH','../../yii2-framework/vendor/');

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/' . VENDOR_PATH . 'autoload.php');
require(__DIR__ . '/' . VENDOR_PATH . 'yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../config/api.php');

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: TRUE");
header('Access-Control-Allow-Methods: GET, POST, PUT, PATCH, OPTIONS, DELETE');


(new yii\web\Application($config))->run();
